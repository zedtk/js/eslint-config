import type { Linter } from "eslint";

const config: Linter.BaseConfig = {
    overrides: [
        {
            files: ["*.ts", "*.tsx"],
            parserOptions: {
                ecmaVersion: "latest" as never,
                project: "**/tsconfig.json",
            },
            extends: [
                "./rulesets/eslint",
                "./rulesets/typescript-eslint",
                "./rulesets/import",
                "./rulesets/unused-imports",
            ],
        },
    ],
};

export = config;
