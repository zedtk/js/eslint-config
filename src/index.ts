import type { Linter } from "eslint";

const config: Linter.BaseConfig = {
    extends: ["./javascript", "./typescript"],
    overrides: [
        {
            files: ["*"],
            extends: ["prettier"],
        },
    ],
};

export = config;
