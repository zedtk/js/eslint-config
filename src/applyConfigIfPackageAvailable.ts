/* eslint-disable no-console */
import { dirname } from "path";

import type { Linter } from "eslint";
import readPkg from "read-pkg";
import semverCompare from "semver-compare";

const lowerVersion: number = -1;

export function applyConfigIfPackageAvailable(
    name: string,
    version: string,
    config: Linter.BaseConfig,
): Linter.BaseConfig {
    let packageDirectory: string = "";

    try {
        packageDirectory = dirname(require.resolve(`${name}/package.json`));
        // eslint-disable-next-line unused-imports/no-unused-vars
    } catch (error: unknown) {
        console.warn(`Consider using package '${name}': npm i -D ${name}`);

        return {};
    }

    const { version: currentVersion } = readPkg.sync({ cwd: packageDirectory });

    if (semverCompare(currentVersion, version) === lowerVersion) {
        console.warn(`Update package '${name}': npm i -D ${name}`);
    }

    return config;
}
