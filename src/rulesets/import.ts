import type { Linter } from "eslint";

import { applyConfigIfPackageAvailable } from "../applyConfigIfPackageAvailable";

const staticAnalysisRules: Partial<Linter.RulesRecord> = {
    "import/no-useless-path-segments": ["error", { noUselessIndex: true }],
    "import/no-absolute-path": "error",
};

const styleGuideRules: Partial<Linter.RulesRecord> = {
    "import/first": "error",
    "import/order": [
        "error",
        {
            alphabetize: {
                order: "asc",
                caseInsensitive: true,
            },
            groups: ["builtin", "external", "internal"],
            "newlines-between": "always",
            pathGroups: [
                {
                    pattern: "@angular/**",
                    group: "builtin",
                    position: "before",
                },
                {
                    pattern: "src/**",
                    group: "internal",
                },
            ],
            pathGroupsExcludedImportTypes: [],
        },
    ],
    "import/newline-after-import": "error",
};

export = applyConfigIfPackageAvailable("eslint-plugin-import", "2.24.0", {
    overrides: [
        {
            files: ["*.js", "*.jsx", "*.ts", "*.tsx"],
            plugins: ["import"],
            extends: ["plugin:import/typescript"],
            rules: {
                ...staticAnalysisRules,
                ...styleGuideRules,
            },
        },
    ],
});
