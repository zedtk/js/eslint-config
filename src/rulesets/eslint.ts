/* eslint-disable line-comment-position */
// eslint v7.32.0
import type { Linter } from "eslint";

const possibleErrors: Partial<Linter.RulesRecord> = {
    "no-await-in-loop": "warn",
    "no-console": [
        "error",
        { allow: ["debug", "info", "error", "time", "timeEnd", "trace"] },
    ],
    "no-loss-of-precision": "error",
    "no-promise-executor-return": "error",
    "no-template-curly-in-string": "error",
    "no-unreachable-loop": "error",
    "no-unsafe-optional-chaining": "error",
    "no-useless-backreference": "error",
    "require-atomic-updates": "error",
};

const bestPractices: Partial<Linter.RulesRecord> = {
    "accessor-pairs": "off",
    "array-callback-return": ["error", { checkForEach: true }],
    "block-scoped-var": "error",
    "class-methods-use-this": "error",
    // eslint-disable-next-line @typescript-eslint/no-magic-numbers
    complexity: ["warn", 3],
    "consistent-return": "error",
    curly: "error", // https://github.com/prettier/eslint-config-prettier#curly
    "default-case": "error",
    "default-case-last": "error",
    "default-param-last": "error",
    "dot-notation": "error",
    eqeqeq: "error",
    "grouped-accessor-pairs": ["error", "getBeforeSet"],
    "guard-for-in": "error",
    "max-classes-per-file": "error",
    "no-alert": "error",
    "no-caller": "error",
    "no-constructor-return": "error",
    "no-div-regex": "error",
    "no-else-return": ["error", { allowElseIf: false }],
    "no-empty-function": "error",
    "no-eq-null": "error",
    "no-eval": "error",
    "no-extend-native": "error",
    "no-extra-bind": "error",
    "no-extra-label": "error",
    "no-implicit-coercion": ["error", { disallowTemplateShorthand: true }],
    "no-implicit-globals": "off",
    "no-implied-eval": "error",
    "no-invalid-this": "error",
    "no-iterator": "error",
    "no-labels": "error",
    "no-lone-blocks": "error",
    "no-loop-func": "error",
    "no-magic-numbers": [
        "error",
        {
            enforceConst: true,
            detectObjects: true,
        },
    ],
    "no-multi-str": "error",
    "no-new": "error",
    "no-new-func": "error",
    "no-new-wrappers": "error",
    "no-nonoctal-decimal-escape": "error",
    "no-octal-escape": "error",
    "no-param-reassign": "error",
    "no-proto": "error",
    "no-restricted-properties": "off", // To complete if needed
    "no-return-assign": "error",
    "no-return-await": "error",
    "no-script-url": "error",
    "no-self-compare": "error",
    "no-sequences": "off", // No-restricted-syntax
    "no-throw-literal": "error",
    "no-unmodified-loop-condition": "error",
    "no-unused-expressions": "error",
    "no-useless-call": "error",
    "no-useless-concat": "error",
    "no-useless-return": "error",
    "no-void": "error",
    "no-warning-comments": "off",
    "prefer-named-capture-group": "error",
    "prefer-promise-reject-errors": "error",
    "prefer-regex-literals": "error",
    radix: "error",
    "require-await": "error",
    "require-unicode-regexp": "error",
    "vars-on-top": "error",
    yoda: ["error", "never", { exceptRange: true }],
};

const variables: Partial<Linter.RulesRecord> = {
    "init-declarations": "error",
    "no-label-var": "error",
    "no-restricted-globals": "off", // To complete if needed
    "no-shadow": ["error", { hoist: "all" }],
    "no-undef-init": "error",
    "no-undefined": "error",
    "no-unused-vars": [
        "error",
        {
            args: "all",
            caughtErrors: "all",
        },
    ],
    "no-use-before-define": "error",
};

const stylisticIssues: Partial<Linter.RulesRecord> = {
    camelcase: "error",
    "capitalized-comments": "error",
    "consistent-this": "error",
    "func-name-matching": "error",
    "func-names": ["error", "as-needed"],
    "func-style": ["error", "declaration", { allowArrowFunctions: true }],
    "id-length": "off",
    "id-match": "off",
    "line-comment-position": "error",
    "lines-around-comment": [
        "error",
        {
            beforeBlockComment: true,
            afterBlockComment: false,
            beforeLineComment: true,
            afterLineComment: false,
            allowBlockStart: true,
            allowBlockEnd: true,
            allowObjectStart: true,
            allowObjectEnd: true,
            allowArrayStart: true,
            allowArrayEnd: true,
            allowClassStart: true,
            allowClassEnd: true,
        },
    ], // https://github.com/prettier/eslint-config-prettier#lines-around-comment
    "lines-between-class-members": "error",
    "max-depth": "error",
    "max-len": "off", // https://github.com/prettier/eslint-config-prettier#no-confusing-arrow
    "max-lines": "warn",
    "max-lines-per-function": "error", // Zbouby
    "max-nested-callbacks": "error", // Zbouby
    "max-params": "off", // Zbouby
    "max-statements": "off", // Zbouby
    "max-statements-per-line": "error",
    "multiline-comment-style": "off",
    "new-cap": "error",
    "no-array-constructor": "error",
    "no-bitwise": "error",
    "no-continue": "warn",
    "no-inline-comments": "off",
    "no-lonely-if": "error",
    "no-mixed-operators": "error", // https://github.com/prettier/eslint-config-prettier#no-mixed-operators
    "no-multi-assign": "error",
    "no-negated-condition": "error",
    "no-nested-ternary": "error",
    "no-new-object": "error",
    "no-plusplus": "off",
    "no-restricted-syntax": [
        "error",
        {
            selector: "SequenceExpression",
            message:
                "The comma operator is confusing and a common mistake. Don’t use it!",
        },
    ], // To complete if needed
    "no-tabs": ["error", { allowIndentationTabs: true }], // https://github.com/prettier/eslint-config-prettier#no-tabs
    "no-ternary": "off",
    "no-underscore-dangle": "error", // Zbouby
    "no-unneeded-ternary": "error",
    "one-var": ["error", "never"],
    "operator-assignment": "error",
    "padding-line-between-statements": [
        "error", // TO COMPLETE
        {
            blankLine: "always",
            prev: "*",
            next: "return",
        },
    ],
    "prefer-exponentiation-operator": "error",
    "prefer-object-spread": "error",
    quotes: [
        "error",
        "double",
        { avoidEscape: true, allowTemplateLiterals: false },
    ],
    "sort-keys": "off",
    "sort-vars": "off",
    "spaced-comment": [
        "error",
        "always",
        {
            exceptions: ["-", "+", "*"],
            markers: ["/"],
        },
    ],
};

const ecmaScript6: Partial<Linter.RulesRecord> = {
    "arrow-body-style": "error",
    "no-confusing-arrow": ["error", { allowParens: false }], // https://github.com/prettier/eslint-config-prettier#no-confusing-arrow
    "no-duplicate-imports": "error",
    "no-restricted-exports": "off", // To complete if needed
    "no-restricted-imports": [
        "error",
        {
            paths: [
                {
                    name: ".",
                    message: "Please import from './<target>' instead",
                },
                {
                    name: "rxjs/Rx",
                    message: "Please import directly from 'rxjs' instead",
                },
                {
                    name: "primeng",
                    message: "Please import from 'primeng/<module>' instead",
                },
            ],
        },
    ],
    "no-useless-computed-key": "error",
    "no-useless-constructor": "error", // Disable for *.store.ts
    "no-useless-rename": "error",
    "no-var": "error",
    "object-shorthand": [
        "error",
        "always",
        {
            avoidQuotes: true,
            avoidExplicitReturnArrows: true,
        },
    ],
    "prefer-arrow-callback": ["error", { allowUnboundThis: false }],
    "prefer-const": "error",
    "prefer-destructuring": [
        "error",
        {
            array: false,
            object: true,
        },
        { enforceForRenamedProperties: true },
    ],
    "prefer-numeric-literals": "error",
    "prefer-rest-params": "error",
    "prefer-spread": "error",
    "prefer-template": "error",
    "sort-imports": "off",
    "symbol-description": "error",
};

const config: Linter.BaseConfig = {
    overrides: [
        {
            files: ["*.js", "*.jsx", "*.ts", "*.tsx"],
            extends: ["eslint:recommended"],
            rules: {
                ...possibleErrors,
                ...bestPractices,
                strict: "off",
                ...variables,
                ...stylisticIssues,
                ...ecmaScript6,
            },
        },
    ],
};

export = config;
