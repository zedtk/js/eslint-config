/* eslint-disable line-comment-position */
// @typescript-eslint/eslint-plugin v4.29.1
import type { Linter } from "eslint";

const rules: Partial<Linter.RulesRecord> = {
    "@typescript-eslint/array-type": ["error", { default: "generic" }],
    "@typescript-eslint/ban-tslint-comment": "error",
    "@typescript-eslint/class-literal-property-style": "error",
    "@typescript-eslint/consistent-indexed-object-style": "error",
    "@typescript-eslint/consistent-type-assertions": "error",
    "@typescript-eslint/consistent-type-definitions": "error",
    "@typescript-eslint/consistent-type-imports": "error",
    "@typescript-eslint/explicit-function-return-type": [
        "error",
        { allowExpressions: true },
    ],
    "@typescript-eslint/explicit-member-accessibility": [
        "error",
        { overrides: { constructors: "no-public" } },
    ],
    "@typescript-eslint/explicit-module-boundary-types": "off", // @typescript-eslint/explicit-function-return-type
    "@typescript-eslint/member-ordering": "error",
    "@typescript-eslint/method-signature-style": "error",
    camelcase: "off",
    "@typescript-eslint/naming-convention": [
        "error",
        {
            selector: "default",
            format: ["camelCase"],
        },
        {
            selector: "variable",
            format: ["camelCase", "UPPER_CASE"],
        },
        {
            selector: "parameter",
            format: ["camelCase"],
            leadingUnderscore: "allow",
        },
        {
            selector: "memberLike",
            modifiers: ["private"],
            format: ["camelCase"],
            leadingUnderscore: "require",
        },
        {
            selector: "typeLike",
            format: ["PascalCase"],
        },
    ],
    "@typescript-eslint/no-base-to-string": "off",
    "@typescript-eslint/no-confusing-non-null-assertion": "error",
    "@typescript-eslint/no-confusing-void-expression": "error",
    "@typescript-eslint/no-dynamic-delete": "error",
    "@typescript-eslint/no-explicit-any": "error",
    "@typescript-eslint/no-extraneous-class": "error",
    "@typescript-eslint/no-implicit-any-catch": "error",
    "@typescript-eslint/no-inferrable-types": "off", // Zbouby { "ignoreParameters": true, "ignoreProperties": true }
    "@typescript-eslint/no-invalid-void-type": "error",
    "@typescript-eslint/no-parameter-properties": "off", // Zbouby
    "@typescript-eslint/no-require-imports": "error",
    "@typescript-eslint/no-type-alias": "off",
    "@typescript-eslint/no-unnecessary-boolean-literal-compare": "error", // Zbouby
    "@typescript-eslint/no-unnecessary-condition": "error",
    "@typescript-eslint/no-unnecessary-qualifier": "error",
    "@typescript-eslint/no-unnecessary-type-arguments": "error",
    "@typescript-eslint/no-unnecessary-type-constraint": "error",
    "@typescript-eslint/no-unsafe-argument": "error",
    "@typescript-eslint/no-unsafe-member-access": "warn",
    "@typescript-eslint/non-nullable-type-assertion-style": "error",
    "@typescript-eslint/prefer-enum-initializers": "error",
    "@typescript-eslint/prefer-for-of": "error",
    "@typescript-eslint/prefer-function-type": "error",
    "@typescript-eslint/prefer-includes": "error",
    "@typescript-eslint/prefer-literal-enum-member": "error",
    "@typescript-eslint/prefer-nullish-coalescing": "error",
    "@typescript-eslint/prefer-optional-chain": "error",
    "@typescript-eslint/prefer-readonly": "error",
    "@typescript-eslint/prefer-readonly-parameter-types": "off", // TODO : A mettre en oeuvre plus tard
    "@typescript-eslint/prefer-reduce-type-parameter": "error",
    "@typescript-eslint/prefer-return-this-type": "error",
    "@typescript-eslint/prefer-string-starts-ends-with": "error",
    "@typescript-eslint/prefer-ts-expect-error": "error",
    "@typescript-eslint/promise-function-async": "error",
    "@typescript-eslint/require-array-sort-compare": "error",
    "@typescript-eslint/restrict-plus-operands": [
        "error",
        { checkCompoundAssignments: true },
    ],
    "@typescript-eslint/restrict-template-expressions": "off", // Zbouby
    "@typescript-eslint/sort-type-union-intersection-members": "error",
    "@typescript-eslint/strict-boolean-expressions": [
        "error",
        {
            allowString: false,
            allowNumber: false,
            allowNullableObject: false,
        },
    ],
    "@typescript-eslint/switch-exhaustiveness-check": "error",
    "@typescript-eslint/typedef": [
        "error",
        {
            memberVariableDeclaration: true,
            parameter: true,
            propertyDeclaration: true,
            variableDeclaration: true,
            variableDeclarationIgnoreFunction: true,
        },
    ],
    "@typescript-eslint/unbound-method": ["error", { ignoreStatic: true }],
    "@typescript-eslint/unified-signatures": "error",
};
const extensionRules: Partial<Linter.RulesRecord> = {
    "default-param-last": "off",
    "@typescript-eslint/default-param-last": "error",
    "dot-notation": "off",
    "@typescript-eslint/dot-notation": [
        "error",
        { allowIndexSignaturePropertyAccess: true },
    ],
    "init-declarations": "off",
    "@typescript-eslint/init-declarations": "error",

    "lines-between-class-members": "off",
    "@typescript-eslint/lines-between-class-members": [
        "error",
        "always",
        { exceptAfterOverload: true },
    ],
    "no-dupe-class-members": "off",
    "@typescript-eslint/no-dupe-class-members": "error",
    "no-duplicate-imports": "off",
    "@typescript-eslint/no-duplicate-imports": "error",
    "no-invalid-this": "off",
    "@typescript-eslint/no-invalid-this": "error",
    "no-loop-func": "off",
    "@typescript-eslint/no-loop-func": "error",
    "no-loss-of-precision": "off",
    "@typescript-eslint/no-loss-of-precision": "error",
    "no-magic-numbers": "off",
    "@typescript-eslint/no-magic-numbers": [
        "error",
        {
            enforceConst: true,
            detectObjects: true,
            ignoreEnums: true,
            ignoreReadonlyClassProperties: true,
        },
    ],
    "no-redeclare": "off",
    "@typescript-eslint/no-redeclare": "error",
    "no-shadow": "off",
    "@typescript-eslint/no-shadow": [
        "error",
        {
            hoist: "all",
            ignoreTypeValueShadow: false,
            ignoreFunctionTypeParameterNameValueShadow: false,
        },
    ],
    "no-throw-literal": "off",
    "@typescript-eslint/no-throw-literal": "error",
    "no-unused-expressions": "off",
    "@typescript-eslint/no-unused-expressions": "error",
    "@typescript-eslint/no-unused-vars": [
        "error",
        {
            args: "all",
            caughtErrors: "all",
        },
    ],
    "no-use-before-define": "off",
    "@typescript-eslint/no-use-before-define": [
        "error",
        { ignoreTypeReferences: false },
    ],
    "no-useless-constructor": "off",
    "@typescript-eslint/no-useless-constructor": "error",
    quotes: "off",
    "@typescript-eslint/quotes": [
        "error",
        "double",
        { avoidEscape: true, allowTemplateLiterals: false },
    ],
    "return-await": "off",
    "@typescript-eslint/return-await": "error",
};

const config: Linter.BaseConfig = {
    overrides: [
        {
            files: ["*.ts", "*.tsx"],
            plugins: ["@typescript-eslint"],
            extends: [
                "plugin:@typescript-eslint/recommended",
                "plugin:@typescript-eslint/recommended-requiring-type-checking",
            ],
            rules: {
                ...rules,
                ...extensionRules,
            },
        },
    ],
};

export = config;
