import { applyConfigIfPackageAvailable } from "../applyConfigIfPackageAvailable";

export = applyConfigIfPackageAvailable(
    "eslint-plugin-unused-imports",
    "1.1.3",
    {
        overrides: [
            {
                files: ["*.js", "*.jsx", "*.ts", "*.tsx"],
                plugins: ["unused-imports"],
                rules: {
                    "unused-imports/no-unused-imports": "error",
                    "unused-imports/no-unused-vars": [
                        "error",
                        {
                            vars: "all",
                            args: "after-used",
                            argsIgnorePattern: "^_",
                            ignoreRestSiblings: false,
                            caughtErrors: "all",
                        },
                    ],
                },
            },
            {
                files: ["*.js", "*.jsx"],
                rules: { "no-unused-vars": "off" },
            },
            {
                files: ["*.ts", "*.tsx"],
                rules: { "@typescript-eslint/no-unused-vars": "off" },
            },
        ],
    },
);
