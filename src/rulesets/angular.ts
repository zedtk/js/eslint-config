// Import { applyConfigIfPackageAvailable } from "../applyConfigIfPackageAvailable";

// Export = applyConfigIfPackageAvailable(
//     "@angular-eslint/eslint-plugin",
//     "0.5.0-beta.4",
//     {
//         Plugins: ["import"],
//         Rules: {
//             "import/newline-after-import": ["error", { count: 1 }],
//             // "import/no-internal-modules": [
//             //     "error",
//             //     {
//             //         allow: [
//             //             "zone.js/**/*",
//             //             "@angular/**/*",
//             //             "primeng/*",
//             //             "rxjs/*",
//             //             "date-fns/*",
//             //             "highcharts/*",
//             //             "highcharts/**/*",
//             //             "pdfmake/**/*",
//             //         ],
//             //     },
//             // ],
//             "import/no-useless-path-segments": [
//                 "error",
//                 { noUselessIndex: true },
//             ],
//             "import/order": [
//                 "error",
//                 {
//                     Alphabetize: {
//                         Order: "asc",
//                         CaseInsensitive: true,
//                     },
//                     Groups: ["builtin", "external", "internal"],
//                     "newlines-between": "always",
//                     PathGroups: [
//                         {
//                             Pattern: "@angular/**",
//                             Group: "builtin",
//                             Position: "before",
//                         },
//                         {
//                             Pattern: "src/**",
//                             Group: "internal",
//                         },
//                     ],
//                     PathGroupsExcludedImportTypes: [],
//                 },
//             ],
//         },
//         Overrides: [
//             /**
//              * -----------------------------------------------------
//              * TYPESCRIPT FILES (COMPONENTS, SERVICES ETC) (.ts)
//              * -----------------------------------------------------
//              */
//             {
//                 Files: ["*.ts"],
//                 Extends: [
//                     "plugin:@angular-eslint/ng-cli-compat",

//                     /**
//                      * NOTE:
//                      *
//                      * We strongly recommend using a dedicated code formatter, like Prettier, and not
//                      * Using a linter to enforce code formatting concerns.
//                      *
//                      * However, for maximum compatibility with how the Angular CLI has traditionally
//                      * Done things using TSLint, this code formatting related add-on config is provided.
//                      */
//                     "plugin:@angular-eslint/ng-cli-compat--formatting-add-on",

//                     /**
//                      * -----------------------------------------------------
//                      * EXTRACT INLINE TEMPLATES (from within Component declarations)
//                      * -----------------------------------------------------
//                      *
//                      * This extra extends is necessary to extract inline templates from within
//                      * Component metadata, e.g.:
//                      *
//                      * @Component({
//                      *  Template: `<h1>Hello, World!</h1>`
//                      * })
//                      * ...
//                      *
//                      * It works by extracting the template part of the file and treating it as
//                      * If it were a full .html file, and it will therefore match the configuration
//                      * Specific for *.html above when it comes to actual rules etc.
//                      *
//                      * NOTE: This processor will skip a lot of work when it runs if you don't use
//                      * Inline templates in your projects currently, and when it runs on a non-Component
//                      * File so there is no great benefit in removing it, but you can if you want to.
//                      */
//                     "plugin:@angular-eslint/template/process-inline-templates",
//                 ],
//                 Rules: {
//                     /**
//                      * Any TypeScript related rules you wish to use/reconfigure over and above the
//                      * Recommended set provided by the @angular-eslint project would go here.
//                      *
//                      * There are some examples below from the @angular-eslint plugin and ESLint core:
//                      */
//                     "@angular-eslint/directive-selector": [
//                         "error",
//                         {
//                             Type: "attribute",
//                             Prefix: "app",
//                             Style: "camelCase",
//                         },
//                     ],
//                     "@angular-eslint/component-selector": [
//                         "error",
//                         { type: "element", prefix: "app", style: "kebab-case" },
//                     ],
//                 },
//             },

//             /**
//              * -----------------------------------------------------
//              * COMPONENT TEMPLATES
//              * -----------------------------------------------------
//              *
//              * If you use inline templates, make sure you you have read the notes on the extends
//              * In the "*.ts" overrides above to understand how they relate to this configuration
//              * Directly below.
//              */
//             {
//                 Files: ["*.html"],
//                 Extends: ["plugin:@angular-eslint/template/recommended"],
//                 Rules: {
//                     /**
//                      * Any template/HTML related rules you wish to use/reconfigure over and above the
//                      * Recommended set provided by the @angular-eslint project would go here.
//                      *
//                      * There is an example below from ESLint core (note, this specific example is not
//                      * Necessarily recommended for all projects):
//                      */
//                     "max-len": ["error", { code: 140 }],
//                 },
//             },
//         ],
//     }
// );
