import type { Linter } from "eslint";

const config: Linter.BaseConfig = {
    overrides: [
        {
            files: ["*.js", "*.jsx"],
            env: {
                node: true,
                es2017: true,
            },
            parserOptions: {
                // eslint-disable-next-line @typescript-eslint/no-magic-numbers
                ecmaVersion: 2018,
                sourceType: "module",
            },
            extends: [
                "./rulesets/eslint",
                "./rulesets/import",
                "./rulesets/unused-imports",
            ],
        },
    ],
};

export = config;
