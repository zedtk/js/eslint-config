# @zedtk/eslint-config

[![GitLab pipeline](https://gitlab.com/zedtk/js/eslint-config/badges/main/pipeline.svg)](https://gitlab.com/zedtk/js/eslint-config/-/commits/main)

[![npm](https://img.shields.io/npm/v/@zedtk/eslint-config?logo=npm)](https://www.npmjs.com/package/@zedtk/eslint-config)
[![Snyk Known Vulnerabilities](https://img.shields.io/snyk/vulnerabilities/npm/@zedtk/eslint-config?logo=snyk)](https://snyk.io/test/npm/@zedtk/eslint-config)

![License](https://img.shields.io/badge/license-MIT-brightgreen)

@zedtk/eslint-config is a set of opinionated configurations for eslint and various plugins.

The configurations can be considered strict and suited for enterprise developement.

## Prerequisites

Before you begin, ensure you have met the following requirements:

-   You have installed `eslint` version 7 or higher

## Installing @zedtk/eslint-config

To install @zedtk/eslint-config, follow these steps:

npm:

```shell
npm i -D @zedtk/eslint-config
```

pnpm:

```shell
pnpm add -D @zedtk/eslint-config
```

## Using @zedtk/eslint-config

To use @zedtk/eslint-config, follow these steps:

.eslintrc.json

```json
{
    ...
    "extends": "@zedtk"
    ...
}
```

Default configuration provides rules for JavaScript and TypeScript based on:

-   eslint
-   @typescript-eslint

And configuration for the following plugins if installed:

-   eslint-plugin-import
-   eslint-plugin-unused-imports

If you are only interested in a subset of the provided configuration, you can extend from:

-   a profile: `"extends": "@zedtk/eslint-config/javascript"`
-   a ruleset: `"extends": "@zedtk/eslint-config/rulesets/unused-imports"`

## License

This project uses the following license: [MIT](https://gitlab.com/zedtk/js/eslint-config/-/blob/main/LICENCE.txt)
