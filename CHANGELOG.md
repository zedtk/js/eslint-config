## [1.0.3](https://gitlab.com/zedtk/js/eslint-config/compare/v1.0.2...v1.0.3) (2021-08-20)


### Bug Fixes

* readme ([828638c](https://gitlab.com/zedtk/js/eslint-config/commit/828638cc4206cf6907e370793c43a9052b1526eb))

## [1.0.2](https://gitlab.com/zedtk/js/eslint-config/compare/v1.0.1...v1.0.2) (2021-08-19)


### Bug Fixes

* pipeline status ([12dfaf3](https://gitlab.com/zedtk/js/eslint-config/commit/12dfaf3de3dba0f5ba8470c5a438aad28e6bba4b))
* readme badges ([8e98eee](https://gitlab.com/zedtk/js/eslint-config/commit/8e98eeea328eb67064fcd90589314260fdd109bb))

## [1.0.1](https://gitlab.com/zedtk/js/eslint-config/compare/v1.0.0...v1.0.1) (2021-08-18)


### Bug Fixes

* license ([3ea20b8](https://gitlab.com/zedtk/js/eslint-config/commit/3ea20b87d8ce901888cd38da6bb2b1cb0d6c1b49))
* readme ([7c2e317](https://gitlab.com/zedtk/js/eslint-config/commit/7c2e317dff8d390a3131fb5bb7d47d2d43cec36a))
